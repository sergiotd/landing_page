<div class="modal hide fade" id="<?php echo $modalId ?>">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<?php $this->load->view($modalHeader); ?>
	</div>
	<div class="modal-body"></div>
	<div class="modal-footer">
		<?php $this->load->view($modalFooter); ?>
	</div>
</div>