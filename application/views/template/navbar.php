<div class="navbar">
	<div class="navbar-inner">
		<a class="brand" href="<?php echo site_url('home') ?>"><?php echo $pageLogo ?></a>
		<!-- Main Links -->
		<ul class="nav">
			<?php foreach ($navbarLinks as $key): ?>
			<li class="<?php echo is_active_page(strtolower(underscore($key)), $navbarActivePage) ?>">
				<?php
				switch ($key) {
				  	case 'BLOG':
						$attr = array(
							'id' => 'navbarBlog',
							'target' => '_blank'
							);
						echo anchor('http://teamupsports.wordpress.com/', $key, $attr);
				  		break;
				  	default:
						$attr = array();
						echo anchor(site_url(strtolower(underscore($key))), $key, $attr);
				  		break;
				}  
				?>
			</li>
			<?php endforeach ?>
		</ul>
		<ul class="nav pull-right">
			<?php if ($this->session->userdata('userId')): ?>
				<li class="<?php echo is_active_page('my_account', $navbarActivePage) ?>">
					<?php 
						echo anchor('my_account', 'MY ACCOUNT', '') 
					?>
				</li>
				<li class="<?php echo is_active_page('logout', $navbarActivePage) ?>">
					<?php echo anchor('logout', 'LOGOUT', '') ?>
				</li>
			<?php else: ?>
				<li class="<?php echo is_active_page('login', $navbarActivePage) ?>">
					<?php 
						$attr = array(
							'id' => 'navbarLogin',
							'data-toggle' => 'modal',
							'data-target' => '#modalLogin'
							);
						echo anchor(strtolower(underscore('login')), 'LOGIN', $attr);
					?>
				</li>
				<li class="<?php echo is_active_page('signup', $navbarActivePage) ?>">
					<?php 
						$attr = array(
							'id' => 'navbarSignup',
							'data-toggle' => 'modal',
							'data-target' => '#modalSignup'
							);
						echo anchor(strtolower(underscore('signup')), 'SIGNUP', $attr); 
					?>
				</li>
			<?php endif ?>
			<li class="dropdown">
				<?php  
				$attr = array(
					'class'=>"dropdown-toggle",
					'data-toggle' => 'dropdown'

					);
				echo anchor('#', '<i class="icon-flag"></i>', $attr);
				?>
				<ul class="dropdown-menu">
					<li><?php echo anchor(current_url().'/set_language/english', 'English', ''); ?></li>
					<li><?php echo anchor(current_url().'/set_language/portuguese', 'Português', ''); ?></li>
					<li><?php echo anchor(current_url().'/set_language/french', 'Francais', ''); ?></li>
					<li><?php echo anchor(current_url().'/set_language/spanish', 'Espanol', ''); ?></li>
					<li><?php echo anchor(current_url().'/set_language/italian', 'Italiano', ''); ?></li>
					<li><?php echo anchor(current_url().'/set_language/german', 'Deutch', ''); ?></li>
				</ul>
			</li>
		</ul>
	</div>
</div>