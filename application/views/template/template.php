<!DOCTYPE html>
<html lang="en">
	<head>
		<title>:: <?php echo $pageTitle ?> - <?php echo $pageSubTitle ?> ::</title>
		<?php $this->load->view($mainCss); ?>
		<?php $this->load->view($pageSpecificCss); ?>
		<?php $this->load->view($mainMetadata); ?>
		<?php $this->load->view($pageSpecificMetadata); ?>
		<!-- Fav Icon -->
		<link rel="icon" type="image/png" href="">
	</head>
	<body>
		<!-- Container -->
		<div class="container">
			<!-- Navigation Bar -->
			<?php $this->load->view($navbar) ?>
			<!-- Main Content -->
			<?php $this->load->view($mainContent) ?>
			<hr>
			<!-- Footer -->
			<?php $this->load->view($footer); ?>

			<!-- LogIn Modal -->
			<?php  
				$config = array(
					'modalId' => 'modalLogin',
					'modalHeader' => 'login/modal_header',
					'modalFooter' => 'login/functions'
				);
				$this->load->view('template/modal', $config, FALSE);
			?>
			<!-- SignUp Modal -->
			<?php	
				$config = array(
					'modalId' => 'modalSignup',
					'modalHeader' => 'signup/modal_header',
					'modalFooter' => 'signup/functions'
				);
				$this->load->view('template/modal', $config, FALSE);

			?>
		</div>

		<!-- SignUp Modal -->
		<?php $this->load->view($mainJs); ?>
		<?php $this->load->view($pageSpecificJs) ?>
	</body>
</html>