<header class="hero-unit">
	<div class="row">
		<div class="span6">  
			<h1>Find Your Perfect Match</h1>
			<p class="lead">Sign Up Now</p>
			<p>
				<a class="btn btn-primary btn-large">
					SignUP NOW
				</a>
			</p>
		</div>
	</div>
</header>