<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('is_active_page'))
{
    function is_active_page($page = '', $activePage = '')
    {
		if ($page == $activePage) {
			return 'active';
		}else{
			return '';
		}
    }
}