<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends MY_Controller {

    public function index()
    {
        $this->load_view();
    }

}

/* End of file blog.php */
/* Location: ./application/controllers/blog.php */