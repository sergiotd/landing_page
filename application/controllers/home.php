<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {

	public function index()
	{
		if (!$this->uri->segment(1)) {
			redirect('home');
		}
		$this->load_view();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */