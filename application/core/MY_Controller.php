<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	var $data = array(
		'mainCss'              => 'template/main_css',
		'pageSpecificCss'      => 'template/page_specific_css',
		'mainMetadata'         => 'template/main_metadata',
		'pageSpecificMetadata' => 'template/page_specific_metadata',
		'metaTagDescription'   => '',	
		'metaTagkeywords'      => '',	
		'pageLogo'             => 'Teamup-Sports',
		'pageTitle'            => 'Teamup-Sports',
		'pageSubTitle'         => 'Sub Titulo',
		'navbar'               => 'template/navbar',
		'navbarLinks'          => array(
			'HOME',
			'PRICING',
			'TOUR',
			'BLOG',
			'ABOUT US',
			'FAQ'
			),
		'navbarActivePage'     => '',
		'mainContent'          => 'template/main_content',
		'mainJs'               => 'template/main_js',
		'pageSpecificJs'       => 'template/page_specific_js',
		'footer'               => 'template/footer'
		);

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('language')) {
			$this->session->set_userdata('language', 'english');
		}else{
			$lang = $this->session->userdata('language');
			$this->lang->load('main', $lang);
		}
		$this->data['mainContent'] = $this->uri->segment(1).'/main_content';
		$this->data['navbarActivePage'] = $this->uri->segment(1);
	}
	
	public function load_view()
    {
    	$this->load->view('template/template', $this->data, FALSE);
    }

    public function set_language($language = '')
    {
    	if ($language != '') {
    		$this->session->set_userdata('language', $language);
    		redirect(strtolower($this->uri->segment(1)));
    	}else{
    		redirect(strtolower($this->uri->segment(1)));
    	}
    }

}

/* End of file mY_Controller.php */
/* Location: ./application/controllers/mY_Controller.php */